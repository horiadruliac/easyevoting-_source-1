<?php /*%%SmartyHeaderCode:9953088605b72c14c485897-42331237%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f8b9d55d2165c33995b29761320883feec921e3' => 
    array (
      0 => './templates/manage/manage.tpl',
      1 => 1531385407,
      2 => 'file',
    ),
    'c34f5fd2cb471064ebd2ae578855609cd85c9a06' => 
    array (
      0 => '/var/www/html/evoting/templates/navbar.tpl',
      1 => 1531385397,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9953088605b72c14c485897-42331237',
  'variables' => 
  array (
    'sessions' => 0,
    'election' => 0,
    'moreelections' => 0,
    'showelections' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5b72c14c569026_74028725',
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b72c14c569026_74028725')) {function content_5b72c14c569026_74028725($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<title>Newcastle University E-Voting: Manage Elections</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<link rel="stylesheet" type="text/css" href="./css/managestyle.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->

<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/manage.js"></script>
	

<!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>
<body>
	
<!--Begin navbar-->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="./index.php">Newcastle University E-Voting</a>
				<div class="nav-collapse collapse">
											<span class="navbar-text pull-right">
							Logged in as carlton123 <a href="./index.php?logout=true" class="navbar-link">(Log out)</a>
						</span>
					                    <ul class="nav">
																		<li><a href="./coordinator.php">Create Election</a></li>
																			<li class="active"><a href="./manage.php">Manage Elections</a></li>
																						<li><a href="./sessionlist.php">View All Elections</a></li>
																<li><a href="./classroombulletinboard.php">Bulletin Board</a></li>
																<li><a  href="./help.php">Help</a></li>
																<li><a href="./contact.php">Contact</a></li>
															</ul>
                </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
         <!--End Nav Bar -->







<div class="container-narrow">
<div class="jumbotron">
<h1>Manage Elections</h1>
<p class="lead">View and administrate your elections</p>
</div>
<div id = "maintext">
		<div>
		<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th>ID</th><th>Status</th><th>Passcode</th><th>Title</th><th>Action</th>
		</tr>	
		</thead>
		<tbody>
				</tbody>
		</table>
		<a id="electionend"> </a>
			</div>
	</div>
</div>


</body>
</html>
<?php }} ?>