<?php
require_once ('./classroominclude.php');
require_once('./auth2.php');
require('./fpdf/fpdf.php');

if(isset($_GET['sessionid'])) {
	class PDF extends FPDF {
              //  function Header() {
              //          $this->Image('./img/ncl-logo.png',10,11,35);
              //          $this->SetFont('Helvetica', '', 20);
              //          $this->Cell(45);
              //          $this->Cell(30,10,"E-voting Passcodes - Election ID: " . $_GET['sessionid'],0,0,'B');
              //          $this->Ln(20);
              //  }

                function Footer() {
                        $this->SetY(-15);
                        $this->SetFont('Helvetica', '', 10);
                        $this->Cell(0,10,'Page ' . $this->PageNo() . ' of {nb}',0,0,'C');
                }
        }	

	$sessionid = $_GET['sessionid'];

	if($query = $db->prepare("SELECT passcodeType,username FROM Sessions WHERE sessionID=?")) {
		
		$query->bindValue(1, isset($sessionid) ? $sessionid : "");
		$query->execute();
		$query->bindColumn(1,$passcodetype);
		$query->bindColumn(2,$username);
			
		$is_individual = null;
		while($row = $query->Fetch()) {
			if($passcodetype == 'individual') {
				$is_individual = true;
			}
		}

		if($is_individual && $username == $_SESSION['USERNAME']) {
			$passcodes = get_passcode_array($db, $sessionid);
			process_pdf($passcodes);
		}

	}else {
		exit;
	}
}else exit;

function get_passcode_array($con, $sessionid) {
	if($query = $con->prepare("SELECT userID FROM Users WHERE sessionID=?")) {
		$query->bindValue(1, isset($sessionid) ? $sessionid : "");
		$query->execute();
		$query->bindColumn(1,$passcode);

		$passcodes = array();
		while($query->Fetch()) {
			array_push($passcodes, $passcode);
		}
		//$query->close();
		return $passcodes;
	}
}

// this is full of magic numbers
// to work with A4 paper
// maybe make it more generic
function process_pdf($passcodes) {
	$pdf = new PDF();
	$pdf->AliasNbPages();
	//$pdf->AddPage();
	$pdf->SetFont('Helvetica','',18);
	for($i=0; $i<count($passcodes); $i+=2) 
	{
		if ($i%16 == 0)
		{
			$pdf->AddPage();
		}
		for ($j=0;$j<min((count($passcodes)-$i),2);$j++)
		{
			$pdf->Cell(90,10,"https://cs-evoting.ncl.ac.uk","LRT");
		}
		$pdf->Ln();
		for ($j=0;$j<min((count($passcodes)-$i),2);$j++)
		{
			$pdf->Cell(90,10,"Election ID: ".$_GET['sessionid'],"LR");
		}
		$pdf->Ln();
		for ($j=0;$j<min((count($passcodes)-$i),2);$j++)
		{
			$pdf->Cell(90,10,"Passcode: ".$passcodes[$i+$j],"LRB",0);
		}
		$pdf->Ln();
		//$pdf->Ln(10);
		//$pdf->Cell(10);	
		//$pdf->Cell(0,0,$passcodes[$i],1,1);
		//$pdf->Cell(75);
		//if ($i+1<count($passcodes))
		//{
		//	$pdf->Cell(0,0,$passcodes[$i+1],1,1);		
		//	$pdf->Cell(140);
		//	if ($i+2<count($passcodes))
		//	{
		//		$pdf->Cell(0,0,$passcodes[$i+2],1,1);
		//		$pdf->Ln(20);
		//	}
	//	}
	}

	$pdf->Output();
}

?>
