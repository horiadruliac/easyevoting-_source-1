<?php 
	
require_once ('./classroominclude.php');

	require_once('auth2.php'); 
	if(!isset($_SESSION['is_admin']) || !$_SESSION['is_admin']) die("You need to be an administrator to view this page!");
	

	$freespace = "";
	$bytes = disk_free_space("/addon/local/");
	$index = 0;
	$type=array("", "K", "M", "G","T");
	while ($bytes > 0)
	{
		$freespace = ($bytes%1024).$type[$index]."B ".$freespace;
		$bytes= (int)$bytes/1024;
		$index++;
		echo($bytes);
	}


	
	$smarty->assign('freespace',$freespace);
	$smarty->assign('highlighted','admin');
	$smarty->assign('username',$_SERVER['PHP_AUTH_USER']);
	$smarty->assign('admin','true');
$smarty->display('admin/admin.tpl');
?>
