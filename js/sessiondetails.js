function confirmfinish(sessionId,questionId) 
{
        var r = confirm("Are you sure you want to finish question "+ questionId+  " in election " + sessionId + "?");

        if(r == true) {
		var url = 'finishquestion.php';
		var form = $('<form style="visibility:hidden" action="' + url + '" method="post">' +
		  '<input type="text" name="sessionid" value="' + sessionId + '" />' +
		  '<input type="text" name="questionid" value="' + questionId + '" />' +		  
		  '</form>');
		$('body').append(form);
		$(form).submit();
	}else{ //Do nothing 
	}
}

//workaround for issue with earlier versions of IE and option.onclick
function coordaction(selectbox)
{
	if (selectbox.options[selectbox.selectedIndex].value!="select")
	{
		eval(selectbox.options[selectbox.selectedIndex].value);
	}
}

