function hidesession(sessionId) {
	var r = confirm("Are you sure you want to hide election " + sessionId + "?");
	if(r == true) {
		var url = "hidesession.php";
		$.post("hidesession.php",{sessionid: sessionId},function(data,result){
    			if(result == 'success'){
				alert("Election " + sessionId + " was successfully hidden. ");
			}else {
				alert("Error during hiding.");
			}
  		});
	}else { //Do nothing
	}
}

function unhidesession(sessionId) {
	var r = confirm("Are you sure you want to unhide election " + sessionId + "?");
	if(r == true) {
		var url = "unhidesession.php";
		$.post("unhidesession.php",{sessionid: sessionId},function(data,result){
    			if(result == 'success'){
				alert("Election " + sessionId + " is now visible to all users. ");
			}else {
				alert("Error during unhiding.");
			}
  		});
	}else { //Do nothing
	}
}
//workaround for issue with earlier versions of IE and option.onclick
function coordaction(selectbox)
{
	if (selectbox.options[selectbox.selectedIndex].value!="select")
	{
		eval(selectbox.options[selectbox.selectedIndex].value);
	}
}
