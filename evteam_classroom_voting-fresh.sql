-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 05, 2017 at 11:37 AM
-- Server version: 10.2.8-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evteam_classroom_voting`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admins`
--

CREATE TABLE `Admins` (
  `Username` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Ballots`
--

CREATE TABLE `Ballots` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `ballotID` int(11) NOT NULL DEFAULT 0,
  `stageOneReceipt` text DEFAULT NULL,
  `stageTwoReceipt` text DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `cancelledFor` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `BallotXML`
--

CREATE TABLE `BallotXML` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `ballotID` int(11) NOT NULL DEFAULT 0,
  `answerNumber` int(11) NOT NULL DEFAULT 0,
  `publicKey` text DEFAULT NULL,
  `restructuredKey` text DEFAULT NULL,
  `yesValue` text DEFAULT NULL,
  `noValue` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `BPA_Coordinators`
--

CREATE TABLE `BPA_Coordinators` (
  `ID` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `BPA_Coordinators`
--

INSERT INTO `BPA_Coordinators` (`ID`) VALUES
('b2052637'),
('ndjc9'),
('nfh19');

-- --------------------------------------------------------

--
-- Table structure for table `BPA_Deletions`
--

CREATE TABLE `BPA_Deletions` (
  `username` varchar(12) NOT NULL,
  `submissionset` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `BPA_Submissions`
--

CREATE TABLE `BPA_Submissions` (
  `ID` int(11) NOT NULL DEFAULT 0,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Election` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Constants`
--

CREATE TABLE `Constants` (
  `constant` text DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Constants`
--

INSERT INTO `Constants` (`constant`, `value`) VALUES
('privateKey', '70188475497184742288584719242854638149309501743080856126695783892127309230141'),
('publicKey', 'BAvMf53ObtOZqghgbeC+u9kuzaL1rAwIyVvWjhAhfIQH3p0GxnciWLmMYDKrj4SeI+w9zp8uKot4FKmELX8CHZw');

-- --------------------------------------------------------

--
-- Table structure for table `ECParameters`
--

CREATE TABLE `ECParameters` (
  `securityLevel` int(11) NOT NULL DEFAULT 0,
  `ParaA` text DEFAULT NULL,
  `ParaB` text DEFAULT NULL,
  `PrimeModulus` text DEFAULT NULL,
  `PointX` text DEFAULT NULL,
  `PointY` text DEFAULT NULL,
  `PointOrder` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ECParameters`
--

INSERT INTO `ECParameters` (`securityLevel`, `ParaA`, `ParaB`, `PrimeModulus`, `PointX`, `PointY`, `PointOrder`) VALUES
(128, 'ffffffff00000001000000000000000000000000fffffffffffffffffffffffc', '5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b', 'ffffffff00000001000000000000000000000000ffffffffffffffffffffffff', '6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296', '4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5', 'ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551');

-- --------------------------------------------------------

--
-- Table structure for table `Guests`
--

CREATE TABLE `Guests` (
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Passwords`
--

CREATE TABLE `Passwords` (
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE `Sessions` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `passcodetype` varchar(20) DEFAULT NULL,
  `passcode` varchar(20) DEFAULT NULL,
  `securityLevel` int(11) DEFAULT NULL,
  `receiptLength` int(11) DEFAULT NULL,
  `created` tinyint(1) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `hidden` tinyint(1) DEFAULT NULL,
  `timeCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type3Answers`
--

CREATE TABLE `Type3Answers` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `userID` int(11) NOT NULL DEFAULT 0,
  `answer` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type3Questions`
--

CREATE TABLE `Type3Questions` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `questiontext` text DEFAULT NULL,
  `finished` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type4Answers`
--

CREATE TABLE `Type4Answers` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `userID` int(11) NOT NULL DEFAULT 0,
  `answer` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type4Questions`
--

CREATE TABLE `Type4Questions` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `questiontext` text DEFAULT NULL,
  `finished` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type12Answers`
--

CREATE TABLE `Type12Answers` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `answerNumber` int(11) NOT NULL DEFAULT 0,
  `answerText` text DEFAULT NULL,
  `votesCast` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type12Questions`
--

CREATE TABLE `Type12Questions` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `questionID` int(11) NOT NULL DEFAULT 0,
  `maxAnswers` int(11) DEFAULT NULL,
  `questionText` text DEFAULT NULL,
  `finished` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Type12Status`
--

CREATE TABLE `Type12Status` (
  `sessionID` int(11) DEFAULT NULL,
  `questionID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `currentChoice` varchar(200) DEFAULT NULL,
  `currentBallot` int(11) DEFAULT NULL,
  `ballotsAllowed` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Usernames`
--

CREATE TABLE `Usernames` (
  `Username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `sessionID` int(11) NOT NULL DEFAULT 0,
  `userID` int(11) NOT NULL DEFAULT 0,
  `allocated` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Ballots`
--
ALTER TABLE `Ballots`
  ADD PRIMARY KEY (`sessionID`,`questionID`,`ballotID`);

--
-- Indexes for table `BallotXML`
--
ALTER TABLE `BallotXML`
  ADD PRIMARY KEY (`sessionID`,`questionID`,`ballotID`,`answerNumber`);

--
-- Indexes for table `BPA_Coordinators`
--
ALTER TABLE `BPA_Coordinators`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `BPA_Deletions`
--
ALTER TABLE `BPA_Deletions`
  ADD PRIMARY KEY (`username`,`submissionset`,`id`);

--
-- Indexes for table `BPA_Submissions`
--
ALTER TABLE `BPA_Submissions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ECParameters`
--
ALTER TABLE `ECParameters`
  ADD PRIMARY KEY (`securityLevel`);

--
-- Indexes for table `Guests`
--
ALTER TABLE `Guests`
  ADD KEY `Username` (`Username`);

--
-- Indexes for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`sessionID`);

--
-- Indexes for table `Type3Answers`
--
ALTER TABLE `Type3Answers`
  ADD PRIMARY KEY (`sessionID`,`questionID`,`userID`);

--
-- Indexes for table `Type3Questions`
--
ALTER TABLE `Type3Questions`
  ADD PRIMARY KEY (`sessionID`,`questionID`);

--
-- Indexes for table `Type4Answers`
--
ALTER TABLE `Type4Answers`
  ADD PRIMARY KEY (`sessionID`,`questionID`,`userID`);

--
-- Indexes for table `Type4Questions`
--
ALTER TABLE `Type4Questions`
  ADD PRIMARY KEY (`sessionID`,`questionID`);

--
-- Indexes for table `Type12Answers`
--
ALTER TABLE `Type12Answers`
  ADD PRIMARY KEY (`sessionID`,`questionID`,`answerNumber`);

--
-- Indexes for table `Type12Questions`
--
ALTER TABLE `Type12Questions`
  ADD PRIMARY KEY (`sessionID`,`questionID`);

--
-- Indexes for table `Usernames`
--
ALTER TABLE `Usernames`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`sessionID`,`userID`);

--
-- Constraints for dumped tables
--
ALTER TABLE `Admins`
  ADD PRIMARY KEY `Username` (`Username`);
ALTER TABLE `Admins`
  ADD CONSTRAINT `Admins_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `Usernames` (`Username`);

--
-- Constraints for table `Guests`
--
ALTER TABLE `Guests`
  ADD CONSTRAINT `Guests_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `Usernames` (`Username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
