<?php
	require_once ('./classroominclude.php');
	require_once ('./auth2.php');
	$smarty->assign('highlighted','coordinator');
	
	if (isset($_SESSION['USERNAME']))
	{
		$smarty->assign('username',$_SESSION['USERNAME']);
	}
	if(isset($_SESSION['is_admin']))
	{
		$smarty->assign('admin','true');
	}	
	else   //limitations on number of elections per day don't apply to admins
	{
		$cutoffTime = date("Y-m-d H-i-s",time()-$SECONDS_IN_DAY);
		$statement = $db->prepare("SELECT count(*) FROM Sessions WHERE username=? AND timeCreated > ?");
		$statement->bindValue(1, $_SESSION['USERNAME']);
		$statement->bindValue(2, $cutoffTime);	
		$statement->execute();
		$row = $statement->Fetch();

 
		if ($row[0] >= 50)
		{	
			//Doesn't really need a nice warning page as the only way to get here is through a system error or a malicious post request
			echo("Can't create more than 50 elections in any 24 hour period");
			exit();
		}
	}
	//echo 'HELLLOOOOO';
	//echo $_SESSION['USERNAME'];
	
	if (isset($_POST['questions']))
	{
				$alllocked = 0;
				if($_POST['alllocked'] == 'yes') $alllocked = 1;

				$election_title = "None";
				if(isset($_POST['electiontitle']) && $_POST['electiontitle'] != '') $election_title = $_POST['electiontitle'];
			
				$passcode_type = $_POST['passcodetype'];
				if($_POST['passcodetype'] == 'group' || $_POST['passcodetype'] == 'Group')
				{
					if($_POST['passcode'] == '' || !isset($_POST['passcode']))
					{
						$passcode_type = "none";
					}
				}
	
					if(!isset($_SESSION['is_admin']))
					{
						if ($_POST['users']>200)
						{
							die("<h1>Elections cannot have more than 200 users</h1>");
						}
					}	
	
				$post_string="<?xml version='1.0'?><classroomvoting><type>generatesession</type><users>".$_POST['users']."</users><multiplier>".$_POST['multiplier']."</multiplier><securitylevel>".$_POST['securitylevel']."</securitylevel><receiptlength>".$_POST['receiptlength']."</receiptlength><passcodetype>".$passcode_type."</passcodetype><passcode>".$_POST['passcode']."</passcode><username>".$_POST['username']."</username><alllocked>".$alllocked."</alllocked><electiontitle><![CDATA[".$election_title."]]></electiontitle><questions>";

				//note  questions is never decremented to save having to renumber, hence we know that it is the highest question number used, not the actual number of questions
				for ($i=1;$i<=$_POST['questions'];$i++)
				{
					if (isset($_POST['questiontype'.$i]))
					{
					
					//$search = array(chr(130), chr(145), chr(146), chr(147), chr(148), chr(151),chr(155),chr(180),chr(226)); 
					$search = array(chr(226).chr(128).chr(153)); 
					//$replace = array("'","'", "'",'"', '"', '-',"'","'","'"); 
					//$search = array("'");
					$replace = array("'");

						$questiontext = $_POST['questiontext'.$i];
						if(is_null($questiontext) || !isset($questiontext))
						{
							die("<h1>Question text must be entered</h1>");
						}
						$questiontext = str_replace($search, $replace, $questiontext);
						//echo($questiontext);
						//$questiontext = urldecode($questiontext);
						//echo(" ");
						//echo($questiontext);
						$post_string=$post_string."<question><questiontype>".$_POST['questiontype'.$i]."</questiontype><questiontext><![CDATA[".$questiontext."]]></questiontext>";

						if ($_POST['questiontype'.$i]=="1" || $_POST['questiontype'.$i]=="2" || $_POST['questiontype'.$i]=="5")
						{

							$post_string=$post_string."<answerset>";
							$counter = 1;
							for ($j=1;$j<=$_POST['question'.$i.'answers'];$j++)
							{
								if (isset($_POST['question'.$i."answer".$j."text"]))
								{
									if ($_POST['question'.$i."answer".$j."text"] == "")
									{
										//$smarty->assign('answer',$j);
										//$smarty->assign('question',$i);										
										//$smarty->display('blankanswer.tpl');
										//exit;
										break;
									}
									$answertext = $_POST['question'.$i."answer".$j."text"];
									$answertext = str_replace($search, $replace, $answertext);
									//$answertext = urldecode($answertext);
					
									$post_string=$post_string."<answer>";
									$post_string=$post_string."<answernumber>".$counter."</answernumber>";
									$post_string=$post_string."<answertext><![CDATA[".$answertext."]]></answertext>";
									$post_string=$post_string."</answer>";								
									$counter++;
								}
							}
							
							
							$post_string=$post_string."</answerset>";
							if ($counter == 1)
							{
							$smarty->assign('question',$i);		
							$smarty->display('questionnoanswer.tpl');
							exit;
							}
						}

						if ($_POST['questiontype'.$i]=="2")
						{
							if (is_numeric($_POST['maxanswers'.$i]))
							{
								$post_string=$post_string."<maxanswers>".$_POST['maxanswers'.$i]."</maxanswers>";
							}
							else
							{
								$smarty->display('nomaxanswer.tpl');
								exit;
							}
							
						}
						
						$post_string = $post_string."</question>";
					
					}
				
				}
				
				$post_string = $post_string."</questions></classroomvoting>";
				
				
				$post_string = trim( preg_replace( '/\s+/', ' ', $post_string ) );
				// post request to signature generator
			//	$url = "http://evoting.ncl.ac.uk/classroomvoting.php";
				$url="http://localhost:4449";
				//$url = ":::80";
				//$url="http://127.0.0.1:25";
				//$url="http://www.easyevoting.com:22";
				//curl_setopt($ch, CURLOPT_PORT, 8088);
				//$header  = "POST HTTP/1.0 \r\n";
				//$header .= "Content-type: text/xml \r\n";
				//$header .= "Content-length: ".strlen($post_string)." \r\n";
				//$header .= "Content-transfer-encoding: text \r\n";
				//$header .= "Connection: close \r\n\r\n"; 
				$header = $post_string;
				

			//	$ch = curl_init();
			//	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
			//	curl_setopt($ch, CURLOPT_URL,$url);
			//	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			//	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $string);

						//$header = $post_string;

						$ch = curl_init();
					    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
						curl_setopt($ch, CURLOPT_URL,$url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, 4);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);
				//echo($header);
				$data = curl_exec($ch); 
				echo "THis is the sessionID  ${data} ";
				//echo "THIS IS A TEST MESSAGE";
				
				if(curl_errno($ch)){
    echo 'Curl error: ' . curl_error($ch);
}
				
				$smarty->assign('sessionID',$data);
				$smarty->display('sessiongenerated.tpl');
				curl_close($ch);

		//		$data = trim($data);
			//	$data = urldecode($data);
    }
	else
	{
		
			$smarty->display('generatesession.tpl');
	
	}

?>
