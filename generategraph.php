<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

		require_once ('./classroominclude.php');
		header("Content-type: image/png");
  		$sessionID = $_GET['sessionid'];
		$questionID = $_GET['questionid'];
		$statement = $db->prepare("SELECT answerText,votesCast FROM Type12Answers WHERE sessionID = ? AND questionID = ? ORDER BY answerNumber");
		$statement->bindValue(1, $sessionID);
		$statement->bindValue(2, $questionID);	
		$statement->execute(); 
	        // read the post data     

		$votes = null;
	        $answers = null;
		
		$counter = 0;
		$found = false; //flag to tell us if we should be getting this from the type3 table instead
		while ($row = $statement->Fetch())
		{
			$found = true;
			$votes[$counter] = $row[1];
			$answers[$counter] = $row[0]." (".$row[1].")";
			$counter++;
		}
		
		if (!$found)	//It's a type 3 so get from there instead.
		{
			$statement = $db->prepare("SELECT answer, COUNT(answer) FROM Type3Answers WHERE sessionID = ? AND questionID = ? GROUP BY answer ORDER BY COUNT(answer) DESC");
			$statement->bindValue(1, $sessionID);
			$statement->bindValue(2, $questionID);	
			$statement->execute();   
			while ($row = $statement->Fetch())
			{	
				$found = true;
				$votes[$counter] = $row[1];
				$answers[$counter] = $row[0]." (".$row[1].")";
				$counter++;
			}
		}
		
		if (!$found)
		{
			$im = imagecreate(1,1); // width , height px
			$white = imagecolorallocate($im,255,255,255); 
		   imagepng($im);
		}
		else
		{
			$largest_value = max($votes);
			$sum_of_values = array_sum($votes);
			
			if ($largest_value == 0)
			{
				//to give us a blank graph rather than an error
				$largest_value = 1;
				$sum_of_values = 1;
			}
			// calculate height from the number of data items
			$height = count($votes)*20 +40;
			//find the width of the longest answer
			
			/*
			$nameWidth = 0;
			for ($i=0;$i<count($answers);$i++)
			{
				$temp = strlen($answers[$i]);
				if ($temp > $nameWidth)
				{
					$nameWidth = $temp;
				}
			
			}
			*/
			$nameWidth = floor(log(count($answers),10))+1;
						
			//calculate the start line x co-ord from the longest name
			$startx = $nameWidth*6 + 10;
			
			$width_multiplier = 200;
			
			// calculate width from the longest name + the standard bar size + a big enough gap 
			// to display the percentage for the largest bar
			$width = $startx + $width_multiplier + 40; 
			
			
			$starty = 20;
			//calculate the bottom co-ord of line
			$endy = (count($answers)+0.75) * 20;
			
				
			
			//temporary values 
			//$height = 255;
			//$width = 320;
			//$startx = 50;
				

			
			$im = imagecreate($width,$height); // width , height px
			$white = imagecolorallocate($im,255,255,255); 
			$black = imagecolorallocate($im,0,0,0);   
			$red = imagecolorallocate($im,255,0,0);   
			$blue = imagecolorallocate($im,0,0,255);
			$green = imagecolorallocate($im,0,255,0);
			$yellow = imagecolorallocate($im,255,255,0);
			$orange = imagecolorallocate($im,255,165,0);		
			$purple = imagecolorallocate($im,255,0,255);		
			$grey = imagecolorallocate($im,100,100,100);
								
		 //   imageline($im, 10, 5, 10, 230, $black);
			imageline($im, $startx, $starty, $startx, $endy, $black);
		

			//$x = 15;   
			//$y = 230;   
			//$x_width = 20;  
			//$y_ht = 0; 
		   

		   
			for ($i=0;$i<count($votes);$i++)
			{
				if ($i%8 == 0)
				{
					$current_colour=$red;
				}
				else if ($i%8 == 1)
				{
					$current_colour = $green;
				}
				else if ($i%8 == 2)
				{
					$current_colour = $blue;
				}			
				else if ($i%8 == 3)
				{
					$current_colour = $yellow;
				}
				else if ($i%8 == 4)
				{
					$current_colour = $purple;
				}
				else if ($i%8 == 5)
				{
					$current_colour = $black;
				}
				else if ($i%8 == 6)
				{
					$current_colour = $orange;
				}	
				else 
				{
					$current_colour = $grey;
				}			
				

			  $width = ($votes[$i]/$largest_value)* $width_multiplier;    
			  
				  imagefilledrectangle($im,$startx,$starty,$startx+$width,$starty+20,$current_colour);
				  //imagestring( $im,2,5,$starty+1,$answers[$i],$black);
				  imagettftext($im,9,0,5,$starty+12,$black, "./FreeSans.ttf", ($i+1));
				  imagettftext($im,9,0,$startx+$width+5,$starty+12,$black, "./FreeSans.ttf", (round(($votes[$i]*100)/$sum_of_values))."%");
				  $starty += 20;  
			 
			}
			imagepng($im);
        }


?> 
