<?php 
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
require_once ('./classroominclude.php');

if(!isset($_SESSION)){
    session_start(); 
} 



if (!isset($_SESSION['USERNAME'])){
	require_once('auth2.php');
}

$SECONDS_IN_DAY = 24*60*60;

$smarty->assign('highlighted','coordinator');

if (isset($_SESSION['USERNAME']))

{

	$smarty->assign('username',$_SESSION['USERNAME']);

}

if(isset($_SESSION['is_admin']))

{

	$smarty->assign('admin','true');

}	

else   //limitations on number of elections per day don't apply to admins

{
	

	$cutoffTime = date("Y-m-d H-i-s",time()-$SECONDS_IN_DAY);

	$statement = $db->prepare("SELECT count(*) FROM Sessions WHERE username=? AND timeCreated > ?");
	$statement->bindValue(1, $_SESSION['USERNAME']);
	$statement->bindValue(2, $cutoffTime);	
	$statement->execute();	
	$row = $statement->Fetch();
	if ($row[0] >= 50)

	{

		$smarty->display("sessionlimit.tpl");

		exit();

	}

}




$users = "";

$passcodeType = "group";

$title = "";

$locked = "no";

$passcode="";

if (isset($_GET['duplicate']))
{

	
//only allow duplicate of the user's own elections, as otherwise it could be used to view the questions in a locked election

	//don't allow duplicate of hidden elections as an administrator has hidden them for a reason

	$duplicate = $_GET['duplicate'];
	$statement = $db->prepare("SELECT count(*) FROM Sessions WHERE Sessions.sessionID=? AND Sessions.username = ? AND hidden = false");
	$statement->bindValue(1, $duplicate);
	$statement->bindValue(2, $_SESSION['USERNAME']);	
	$statement->execute();	
	$row = $statement->Fetch();
	if ($row[0]==0)

	{

		$smarty->display("noauth.tpl");

		exit();

	}


	$statement = $db->prepare("SELECT count(*) FROM Users WHERE Users.sessionID=?");
	$statement->bindValue(1, $duplicate);	
	$statement->execute();	
	$row = $statement->Fetch();
	$users = $row[0];	

	$statement = $db->prepare("SELECT passcodeType,passcode,title,locked FROM Sessions WHERE Sessions.sessionID=?");
	$statement->bindValue(1, $duplicate);	
	$statement->execute();	
	$row = $statement->Fetch();
                

	if ($row[0] == "individual")

	{
		
		$passcodeType = "individual";
			
	}
		
	
if ($row[1] == "none")
	
	{
	
		$passcode = "";
			
	}
		
        $title = $row[2];
        
	if ($row[3])
	
	{
					
		$locked = "yes";
		
	}
		
	else
				
	{
			
			$locked = "no";

	}
		




	$questions = null;


	
//loop through type12 questions

	$statement = $db->prepare("SELECT questionID,questiontext,maxAnswers FROM Type12Questions WHERE Type12Questions.sessionID=? ORDER BY questionID");
	$statement->bindValue(1, $duplicate);	
	$statement->execute();
        while ($row = $statement->Fetch())

	{
		
		$questions[$row[0]]['text']=$row[1];
	
		if ($row[2] > 1)
	
		{
						
			$questions[$row[0]]['type']="2";
				
		}

		else
		
		{
			
			$questions[$row[0]]['type']="1";
	
		}

		// make it easy to get the question number out in a foreach loop in smarty
	
		$questions[$row[0]]['number']=$row[0];

		$questions[$row[0]]['maxanswers']=$row[2];
	
		//pull in correct answers
	
		$statement2 = $db->prepare("SELECT answerNumber,answerText FROM Type12Answers WHERE Type12Answers.sessionID=? AND Type12Answers.questionID=? ORDER BY answerNumber");
		$statement2->bindValue(1, $duplicate);
		$statement2->bindValue(2, $row[0]);	
		$statement2->execute();					

	
		while ($row2 = $statement2->Fetch())
	
		{
	

			$questions[$row[0]]['answers'][$row2[0]]['text']=$row2[1];
			
			$questions[$row[0]]['answers'][$row2[0]]['number']=$row2[0];
	
		}
	
		$questions[$row[0]]['numanswers']=count($questions[$row[0]]['answers']);
		
			
		

	}
	

	
//loop through type3 questions

	$statement = $db->prepare("SELECT questionID,questiontext FROM Type3Questions WHERE Type3Questions.sessionID=? ORDER BY questionID");
	$statement->bindValue(1, $duplicate);	
	$statement->execute();	

        while ($row = $statement->Fetch())
	
	{
	
		$questions[$row[0]]['text']=$row[1];
		
		$questions[$row[0]]['type']="3";
	
		// make it easy to get the question number out in a foreach loop in smarty
	
		$questions[$row[0]]['number']=$row[0];
		
		$questions[$row[0]]['maxanswers']="";
		
		$questions[$row[0]]['answers'][1]['text']="";
		
		$questions[$row[0]]['answers'][2]['text']="";
		
		$questions[$row[0]]['answers'][3]['text']="";
		
		$questions[$row[0]]['answers'][4]['text']="";
		
		$questions[$row[0]]['answers'][1]['number']="1";
	
		$questions[$row[0]]['answers'][2]['number']="2";
	
		$questions[$row[0]]['answers'][3]['number']="3";
	
		$questions[$row[0]]['answers'][4]['number']="4";
		
		$questions[$row[0]]['numanswers'] ="4";
			
	}
				




	
//loop through type4 questions


	$statement = $db->prepare("SELECT questionID,questiontext FROM Type4Questions WHERE Type4Questions.sessionID=? ORDER BY questionID");
	$statement->bindValue(1, $duplicate);	
	$statement->execute();		
        while ($row=$statement->Fetch())

	{
		
		$questions[$row[0]]['text']=$row[1];
		
		$questions[$row[0]]['type']="4";
		
		// make it easy to get the question number out in a foreach loop in smarty
	
		$questions[$row[0]]['number']=$row[0];
		
		$questions[$row[0]]['maxanswers']="";
		
		$questions[$row[0]]['answers'][1]['text']="";
		
		$questions[$row[0]]['answers'][2]['text']="";
		
		$questions[$row[0]]['answers'][3]['text']="";
		
		$questions[$row[0]]['answers'][4]['text']="";
		
		$questions[$row[0]]['answers'][1]['number']="1";
		
		$questions[$row[0]]['answers'][2]['number']="2";
	
		$questions[$row[0]]['answers'][3]['number']="3";
	
		$questions[$row[0]]['answers'][4]['number']="4";
		
		$questions[$row[0]]['numanswers'] ="4";
			
	}
	
}

else

{

	$questions[1]['type']="1";

	$questions[1]['text']="";

	$questions[1]['maxanswers']="";

	$questions[1]['number']="1";

	$questions[1]['answers'][1]['text']="";

	$questions[1]['answers'][2]['text']="";

	$questions[1]['answers'][3]['text']="";

	$questions[1]['answers'][4]['text']="";

	$questions[1]['answers'][1]['number']="1";

	$questions[1]['answers'][2]['number']="2";

	$questions[1]['answers'][3]['number']="3";

	$questions[1]['answers'][4]['number']="4";

	$questions[1]['numanswers'] ="4";

}
	
$smarty->assign('users',$users);
	
$smarty->assign('passcodetype',$passcodeType);

$smarty->assign('sessionpasscode',$passcode);
	
$smarty->assign('sessionquestions',$questions);
	
$smarty->assign('title',$title);
	
$smarty->assign('locked',$locked);
	
$smarty->assign('numofquestions',count($questions));
	
$smarty->display('coordinator/coordinator.tpl');

?>


