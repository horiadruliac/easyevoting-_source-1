<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

   	require_once ('./include.php'); 



	if (isset($_GET["id"]))
  	{
		$id = mysql_real_escape_string($_GET['id']);
		$query=mysql_query("SELECT * FROM Election WHERE ID=".$id);
		$row = mysql_fetch_row($query);
		if ($row == false)
		{
			  generateMainPage($smarty);
		}
		else
		{
			if (time() < strtotime($row[3]))
			{
				//do stuff for election not started yet
				$smarty->assign('election',$row[1]);
				$smarty->display('BPA/notstarted.tpl');
			}
			else if (time() > strtotime($row[4]))
			{
				//do stuff for election ended
				//get submission page number
				$query2 = mysql_query("SELECT ID FROM Submissions WHERE Election = ".$id);
				$row2 = mysql_fetch_row($query2);
				$submissionset = $row2[0];
				$smarty->assign('submissionset',$submissionset);
				
				$smarty->assign('electionnumber',$id);
				$smarty->assign('election',$row[1]);
				$smarty->assign('finishdate',$row[4]);
				$smarty->display('BPA/finishedec.tpl');
			}
			else
			{
				$query2=mysql_query("SELECT COUNT(*) FROM Ballots_".$id);
				$row2 = mysql_fetch_row($query2);

				if ($row2[0] < ($row[5] * $row[6]))
				{
					//election not started because not all ballots have yet been generated
					$smarty->display('BPA/notstarted.tpl');
					
				}
 				else
				{
					//get submission page number
					$query2 = mysql_query("SELECT ID FROM Submissions WHERE Election = ".$id);
					$row2 = mysql_fetch_row($query2);
					$submissionset = $row2[0];
					$smarty->assign('submissionset',$submissionset);
				
					//show as finisihed if time isn't up but all voters have voted
					$query2=mysql_query("SELECT COUNT(*) FROM Voters_".$id." WHERE Status != 3 AND Status != 4");
					$row2 = mysql_fetch_row($query2);
					if ($row2[0] == 0)
					{	
										$smarty->assign('electionnumber',$id);
										$smarty->assign('election',$row[1]);
										$smarty->assign('finishdate',"voter");
										$smarty->display('finishedec.tpl');
					}
					else
					{
						//do stuff for ongoing election
						$smarty->assign('electionnumber',$id);
						$smarty->assign('election',$row[1]);
						$smarty->assign('description',$row[2]);
						$smarty->display('BPA/ongoingec.tpl');
					}
				}
			}

		}

	}
	else
	{
		generateMainPage($smarty);	
			
			
			
	}
	
	
function generateMainPage($smarty)
{
	$submissionPages = null;
			$counter = 0;
			//get all submission pages
                        global $db;
			$statement = $db->prepare("SELECT ID, StartDate,EndDate,Title,Election FROM BPA_Submissions ORDER BY EndDate DESC");
			//loop through
                        $statement->execute();		

                        while ($row = $statement->Fetch())
			{
                                //if submission end date after current date/time then add to $submissions as open for submissions with an end date
				if (strtotime($row[2]) >= time())
				{
					$submissionPages[$counter]['id'] = $row[0];
					$submissionPages[$counter]['enddate'] = $row[2];
					$submissionPages[$counter]['title'] = $row[3];
					$submissionPages[$counter]['type'] = "opensubmission";
					$counter++;
				}
				//if submission end date before current date/time check if election created
				else
				{
					// if election exists then check end date
					if ($row[4] != null)
					{
						$statement2 = $db->prepare("SELECT StartTime,EndTime FROM Election WHERE ID = :id");
                                                $statement2->bindValue(":id", $row[4], PDO::PARAM_INT);
                                                $statement2->execute();
						if ($row2 = $statement2->Fetch())
						{
							//check if in election end date in future
							if (strtotime($row2[1]) >= time())
							{
								//check if election start date in future
								if (strtotime($row2[0]) >= time())
								{
									$submissionPages[$counter]['id'] = $row[4];
									$submissionPages[$counter]['enddate'] = $row[2];
									$submissionPages[$counter]['startdate'] = $row2[0];									
									$submissionPages[$counter]['title'] = $row[3];
									$submissionPages[$counter]['type'] = "electiondue";
									$counter++;
								}
								else
								{
									$submissionPages[$counter]['id'] = $row[4];
									$submissionPages[$counter]['enddate'] = $row2[1];								
									$submissionPages[$counter]['title'] = $row[3];
									$submissionPages[$counter]['type'] = "openelection";
									$counter++;
								}
					
							}
							//if ended 					
							else
							{
									$submissionPages[$counter]['id'] = $row[4];
									$submissionPages[$counter]['enddate'] = $row2[1];								
									$submissionPages[$counter]['title'] = $row[3];
									$submissionPages[$counter]['type'] = "electionended";
									$counter++;							
							}


					
						}
						
					}

					else
					{

							$submissionPages[$counter]['id'] = $row[0];
							$submissionPages[$counter]['enddate'] = $row[2];
							$submissionPages[$counter]['title'] = $row[3];
							$submissionPages[$counter]['type'] = "noelectionyet";
							$counter++;

					}
				}
			}
			$smarty->assign('submissionpages',$submissionPages);
			$smarty->display('BPA/welcome.tpl');
}

?> 
