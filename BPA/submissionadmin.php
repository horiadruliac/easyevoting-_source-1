<?php 
	require_once ('./include.php');
	require_once ('./auth.php');
	require_once ('loadproperties.php');
	//echo("This page is running in test mode and is not currently secure");
	
	//we know the username from using authentication in include.php we just need to know if this user is an election administrator

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	$statement = $db->prepare("SELECT * FROM BPA_Coordinators WHERE ID =?");
	$statement->bindValue(1, $_SERVER['PHP_AUTH_USER']);	
	$statement->execute();		
	$row = $statement->Fetch();
	if ($row == false)
	{
 		//not an administrator so display polite page saying that the user is currently not an administrator for any elections
		$smarty->display('BPA/notcoordinator.tpl');
	}
	else
	{
	
			if (isset($_GET['option']) && $_GET['option'] == "delete" && isset($_GET['submission']) && ctype_digit($_GET['submission']))
			{
				//Original: $submission = mysql_real_escape_string($_GET['submission']);
				$submission = $_GET['submission'];
				if (isset($_GET['sure']) && $_GET['sure'] == "yes")
				{
					$statement = $db->prepare("DELETE FROM BPA_Submissions WHERE ID =?");
					$statement->bindValue(1, $submission);	
					$statement->execute();		
					$statement = $db->prepare("DROP TABLE BPA_Papers_".$submission);	
					$statement->execute();						 	
				}
				else
				{
					$smarty->assign('submission',$submission);
					$smarty->display('BPA/confirmdelete.tpl');
					exit;
					
				}
			}
			// 
			if (isset($_GET['option']) && $_GET['option'] == "end" && isset($_GET['submission']) && ctype_digit($_GET['submission']))
			{
				//Original: $submission = mysql_real_escape_string($_GET['submission']);
				
				$submission = $_GET['submission'];
				if (isset($_GET['sure']) && $_GET['sure'] == "yes")
				{
					$statement = $db->prepare("SELECT EndDate FROM BPA_Submissions WHERE ID =?");
					$statement->bindValue(1, $submission);	
					$statement->execute();
					$row = $statement->Fetch();	

					if (time() < strtotime($row[0]))
					{
						$statement = $db->prepare("UPDATE BPA_Submissions SET EndDate = ? WHERE ID =?");
						$statement->bindValue(1, date( 'Y-m-d H:i:s',time()-1));
						$statement->bindValue(2, $submission);	
						$statement->execute();

					}
					
				}
				else
				{
					$smarty->assign('submission',$submission);
					$smarty->display('BPA/confirmend.tpl');
					exit;			
				
				}
			}
			
			$statement = $db->prepare("SELECT * FROM BPA_Submissions ORDER BY ID DESC");
			$statement->execute();
	
			$row = $statement->Fetch();
			if ($row == false)
			{
				//has no elections created so display polite page saying that the user is currently not an administrator for any elections
				$smarty->display('BPA/submissionpagelist.tpl');
			}
			else
			{
				//loop through elections where this user is admin
			
				$submissions[0]['id'] = $row[0];

				if (time() > strtotime($row[2]))
				{
						$submissions[0]['enddate'] = "Finished";
				}
				else
				{
					$submissions[0]['enddate'] = $row[2];
				}
				$submissions[0]['startdate'] = $row[1];
				//I removed this from line below (Ehsan) ---> "http://".$baseurl.
				//Original: $submissions[0]['viewurl'] = "http://".$baseurl."/BPA/viewsubmission.php?submissionset=".$row[0];
				$submissions[0]['viewurl'] = "/BPA/viewsubmission.php?submissionset=".$row[0];
				$submissions[0]['name'] = $row[3];
				if(is_null($row[4]))
				{
					$submissions[0]['delete']=true;
					//check if past end date and if so then add create election option
					if (time() > strtotime($row[2]))
					{
						$submissions[0]['createelection']=true;
						
					}
				}
				else
				{
					$submissions[0]['electioncreated']=true;
					//check if the associated election has finished
					$statement = $db->prepare("SELECT EndTime FROM BPA_Election WHERE ID =?");
					$statement->bindValue(1, $row[4]);	
					$statement->execute();
					$row2 = $statement->Fetch();
					
					if (time() > strtotime($row2[0]))
					{
						$submissions[0]['delete']=true;
					}
				}

				$counter = 1;
				while ($row = $statement->Fetch())
				{
					$submissions[$counter]['id'] = $row[0];
					$submissions[$counter]['name'] = $row[3];
					if (time() > strtotime($row[2]))
					{
						$submissions[$counter]['enddate'] = "Finished";
					}
					else
					{
							$submissions[$counter]['enddate'] = $row[2];
					}
					$submissions[$counter]['startdate'] = $row[1];

					// Original: $submissions[$counter]['viewurl'] = "http://".$baseurl."/BPA/viewsubmission.php?submissionset=".$row[0];
					$submissions[$counter]['viewurl'] = "/BPA/viewsubmission.php?submissionset=".$row[0];
					
					if(is_null($row[4]))
					{
						$submissions[$counter]['delete']=true;
						//check if past end date and if so then add create election option
						if (time() > strtotime($row[2]) && $row[4] == null)
						{
							$submissions[$counter]['createelection']=true;
						}
					}
					else
					{
						$submissions[$counter]['electioncreated']=true;
						//check if the associated election has finished
						$statement2 = $db->prepare("SELECT EndTime FROM BPA_Election WHERE ID = ?");
						$statement2->bindValue(1, $row[4]);	
						$statement2->execute();
						$row2 = $statement2->Fetch();
						if (time() > strtotime($row2[0]))
						{
							$submissions[$counter]['delete']=true;
						}
						
					}
					$counter ++;
				}
					
				//pass values to template including whether election is fully generated
				$smarty->assign('submissionpages',$submissions);
				$smarty->display('BPA/submissionpagelist.tpl');
			}
				
	}
	

			
?>