<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

	require_once ('./include.php');
	require_once ('./auth.php');
	//echo("This page is operating in test mode and is not secure");
	

	$userID =$_SERVER['PHP_AUTH_USER']; 
		
	//set some stuff here to show title
		
	if (!isset($_GET['id']))
	{
						
		$smarty->display('BPA/invalidsubmissionnumber.tpl');
	}
	else
	{
		$id = ctype_digit($_GET['id']) ? ($_GET['id']) : "0";
		$statement = $db->prepare("SELECT * FROM BPA_Submissions WHERE ID = ?");
		$statement->bindValue(1, $id);	
		$statement->execute();
		$row = $statement->Fetch();
		if (!$row)
		{
			
			$smarty->display('BPA/invalidsubmissionnumber.tpl');
				
		}
		else
		{
			if (time() < strtotime($row[1]))
			{
				//submission period hasn't opened yet
				$smarty->display('BPA/submissionnotstarted.tpl');
			}
			else
			{	
				if (!isset($_GET['paperid']))
				{
				
					//display list of existing papers and check if users wants to change one of them or submit
					$statement2 = $db->prepare("SELECT * FROM ? WHERE Submitter = ?");
					$statement2->bindValue(1, "BPA_Papers_".$id);	
					$statement2->bindValue(2, $userID);
					$statement2->execute();		
					$counter = 0;
					$papers = null;
					while ($row2 = $statement2->Fetch())
					{
						//assign papers to list
						$papers[$counter]['id'] = $row2[0];
						$papers[$counter]['title'] = $row2[1];
						$counter++;
					}
					
					// display smarty page offering to allow user to change any paper on list or submit a new paper
					$smarty->assign('papers',$papers);
					$smarty->assign('submissionset',$id);
					$smarty->display('BPA/choosepaper.tpl');
				}
				else
				{
					if (isset($_GET['option']) && $_GET['option']=="delete")
					{
						if(isset($_GET['sure']) && $_GET['sure'] == "yes" && ctype_digit($id))
						{
							//do delete (make sure this is the user's paper)
							$statement2 = $db->prepare("DELETE FROM BPA_Papers_".$id." WHERE ID = ? AND Submitter = ?");
							//$statement2->bindValue(1, "BPA_Papers_".$id);
							//Original: $statement2->bindValue(2, mysql_real_escape_string($_GET['paperid']));	
							$statement2->bindValue(1, $_GET['paperid']);	
							$statement2->bindValue(2, $userID);
							$statement2->execute();	

										
							//display remaining papers
							$statement2 = $db->prepare("SELECT ID,Title FROM ? WHERE Submitter = ?");
							$statement2->bindValue(1, "BPA_Papers_".$id);
							$statement2->bindValue(2, $userID);
							$statement2->execute();	
							$counter = 0;
							$papers = null;
							while ($row2 = $statement2->Fetch())
							{
								//assign papers to list
								$papers[$counter]['id'] = $row2[0];
								$papers[$counter]['title'] = $row2[1];
								$counter++;
							}
					
							// display smarty page offering to allow user to change any paper on list or submit a new paper
							$smarty->assign('papers',$papers);
							$smarty->assign('submissionset',$id);
							$smarty->display('BPA/choosepaper.tpl');
						}
						else
						{
							//display confirm page
							$smarty->assign('submissionset',$id);
							//Original: $smarty->assign('paperid',mysql_real_escape_string($_GET['paperid']));

							$smarty->assign('paperid',$_GET['paperid']);
							$smarty->display('BPA/confirmdeleteuserpaper.tpl');
						}
					
					
					}
					else
					{
						if ($_GET['paperid'] == -1)
						{
							//wants to submit a new paper
							$smarty->assign('submission',$id);
							$smarty->assign('submitter',$userID);
							if (time() > strtotime($row[2]))
							{
								$smarty->display('BPA/submissionfinished.tpl');
								exit;
							}
							//else
							{
								$smarty->display('BPA/submitpaper.tpl');
								exit;
							}
					
					
						}
						$statement2 = $db->prepare("SELECT ID,Title,BibDetails,Testimony, Submitter FROM BPA_Papers_$id WHERE ID = :pid");
						$statement2->bindValue(":pid", $_GET['paperid'], PDO::PARAM_INT);
						$statement2->execute();	

						$row2 = $statement2->Fetch();
						if (!$row2)
						{
							//display an error message
							echo("Invalid paper id");
						}	
						else
						{
							if ($row2[4] == $userID)
							{
								if (time() > strtotime($row[2]))
								{
							
									//page that lets user view but not change submission (maybe the same as view paper page for voter)
									$smarty->assign('submissionset',$id);
									$smarty->assign('id',$row2[0]);								
									$smarty->display('BPA/submissionclosed.tpl');
								}
								else
								{
									//page that lets user view and change submission
									
									//get all previous submission information
									$smarty->assign('id',$row2[0]);	
									$smarty->assign('submission',$id);								

									$smarty->assign('title',$row2[1]);
									$smarty->assign('bibdetails',$row2[2]);
									$smarty->assign('testimony',$row2[3]);								
									$smarty->display('BPA/changepaper.tpl');
								}
							}
							else
							{
								$smarty->assign('submission',$id);	
								$smarty->display('BPA/nochange.tpl');
							}
						}
					}
				}
				
			}
		}		
	}
			
				

	

			
?>