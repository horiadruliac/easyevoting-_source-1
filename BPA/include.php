<?php
	require_once("./loadproperties.php");
	require_once('../Smarty-3.1.8/libs/Smarty.class.php');
	date_default_timezone_set("Europe/London");
	//smarty initialisation code
	$smarty = new Smarty;
	$smarty->template_dir = '../templates';
	$smarty->config_dir = '../config';
	$smarty->cache_dir = '../smarty_cache';
	$smarty->compile_dir = '../templates_c';
    	$dsn = "mysql:dbname=$databasename;host=$baseurl;port=$dbport";

	$db = null;

	try {
    $db = new PDO($dsn, $databaseusername, $databasepassword); // also allows an extra parameter of configuration
}
	catch(PDOException $e) {
    die('Could not connect to the database:<br/>' . $e);
}


?>