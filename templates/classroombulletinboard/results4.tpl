<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle E-voting: Results Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Results Page Session {$sessionID} Question {$questionID} </h1>
</div>
<div class="pagination-centered">

<h2>{$questiontext} </h2>
<h3>Results</h3>

{if $finished == false}
<p>Voting has not yet finished so more results may be added.</p>
{/if}

<div class="pagination-centered">
<table class="table">
<tr>
<th class="votingoptions"> Answer </th> 
</tr>
{foreach from=$results item = result}
<tr> <td class="votingoptions"> {$result.answer} </td> </tr>
{/foreach} 
</table>
</div>
<p><em>Note: Type 4 questions do not have ballots as they accept free text input.</em></p>
</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
</body>
</html>
