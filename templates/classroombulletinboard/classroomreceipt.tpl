<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Newcastle E-voting: Bulletin Board Page for Ballot {$ballotNumber}</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Bulletin Board for Ballot {$ballotNumber}</h1>
</div>
<div class="pagination-centered">
<h2>Types of Vote</h2>
<ul>
<li>
<strong>Confirmed</strong>: Voter confirmed their choice 
</li>
<li>
<strong>Cancelled</strong>: Voter cancelled their choice
</li>
<li>
<strong>Unused</strong>: Vote was not used and was automatically cancelled by the system
</li>
<li>
<strong>In Progress</strong>: Vote is still in progress. This will change once the voter cancels/confirms. </p>
</li>
</ul>
<br />

<div>
<p class="lead">Session {$sessionID}</p>
<p class="lead">Question {$questionID}</p>
<p class="lead">Ballot Number {$ballotNumber}</p>
<p class="lead">
{if $type=="confirm"}
Confirmed vote. 
{/if}
{if $type=="audit"}
Cancelled (Answer: {$cancelchoice})
{/if}
{if ($type=="unused")}
Unused - automatically cancelled
{/if}
{if ($type=="inprogress")}
In Progress
{/if}
</p>
<p class="lead">Receipt: <strong id='receipt'>{$receiptString}</strong></p>
</div>

</div>
</div>
<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/splitter.js"></script>
</body>
</html>
