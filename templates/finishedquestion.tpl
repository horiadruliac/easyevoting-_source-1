<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Finish Question Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>All Question Finished</h1>
</div>
<div class="pagination-centered">
<h4>This question has been succesfully finished and all unused ballots are being cancelled.  The results are now available.</h4>
<a style="margin-top:30px;" class="btn btn-large btn-primary" href="manage.php">Back to Manage Page</a>
</div>
</div>
</body>
</html>
