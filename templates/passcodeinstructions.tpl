<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Voting Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>Passcode Preparation Instructions</h1>
</div>
<div id = "maintext">
<ol>
<li>Save the xml from the download passcodes link</li>
<li>Open Microsoft Excel.</li>
<li>Go to the file menu and choose open.</li>
<li>Open the xml file (you may have to change the filter to all files or .xml)</li>
<li>Choose import as xml table.</li>
<li>Save this Excel sheet</li>
<li>Open the passcode word document (a copy is available <a href = "passcodes.doc">here</a>).</li>
<li>When prompted to choose a data source, choose the Excel sheet you saved in step 6.</li>
<li>Go to the mailings menu and choose finish and merge.</li>
<li>Print and cut the passcodes.</li>
</ol>
</div>
</div>
</body>
</html>
