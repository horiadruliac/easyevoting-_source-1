 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Classroom Voting</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="../css/ie8.css">	  
    <![endif]-->
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Question Already Answered</h1>
</div>
<div class="pagination-centered">
<h2>This question has already been answered!</h2>
{if $questionID < $questions}
<div class="row">
<form name='myform' action='login.php' method='post'>
<input style="margin:10px;" class="btn btn-large btn-primary" type='submit' value='Next Question' />
<input type='hidden' name='sessionID' value='{$sessionID}'>
<input type='hidden' name='userID' value='{$userID}'>
<input type='hidden' name='questions' value='{$questions}'>
<input type='hidden' name='publicKey' value='{$publicKey}'>
<input type='hidden' name='nextQuestion' value='{$questionID+1}'>
<input type='hidden' name='receiptLength' value='{$receiptLength}'>
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
</form>
</div>
{else}
</h2>To answer another session click <a href='index.php'>here</a>.</h2>
{/if}
</div>
</div>
</div>
</body>
</html>
