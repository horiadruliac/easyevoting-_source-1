<!DOCTYPE html> 
<html>
<head>
<title>Newcastle E-Voting</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<link rel="stylesheet" type="text/css" href="./css/blue.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>

{include file="./navbar.tpl"}
<div class="container-narrow">
<!-- End to be added -->
<div class="jumbotron">

<h1>Question {$questionID}/{$questions}</h1>
</div>
<div class="pagination-centered">

<div class="question">
<h2>{$questionText}</h2>

<form id="theform" class="form-horizontal" name="myform" action="login.php" method="post">

{foreach from = $answers item=answer}
<div class="row">
<span><label for="checkbox{$answer.number}" class="option" style="display:block"><input id="checkbox{$answer.number}" style="margin-bottom:5px" class="questionoption" type="radio" name="answer1" value="{$answer.number}"> {$answer.answer}</label>  </span>
</div>
{/foreach}

<input type="hidden" name="sessionID" value="{$sessionID}">
<input type="hidden" name="userID" value="{$userID}">
<input type="hidden" name="questions" value="{$questions}">
<input type="hidden" name="publicKey" value="{$publicKey}">
<input type="hidden" name="nextQuestion" value="{$questionID}">
<input type="hidden" name="questionType" value="{$questionType}">
<input type="hidden" name="questionText" value="{$questionPass}">
<input type="hidden" name="maxanswers" value="{$maxanswers}">
<input type="hidden" name="possibleanswers" value="{$possibleanswers}">
<input type="hidden" name="previousreceipts" value="{$previousreceipts}">
<input type="hidden" name="answerstring" value="">
{foreach from = $urlanswers item=answer}
<input type="hidden" name="answertext{$answer.number}" value="{$answer.answer}">
{/foreach}
<input type="hidden" name="receiptLength" value="{$receiptLength}">
</form>
<button id="submitButton" class="btn btn-primary btn-large">Submit</button>
</div>
</div>
</div>
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/icheck.js"></script>
<script>
//Validation
$(document).ready(function(){

                 if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|MSIE [67]/i.test(navigator.userAgent)) ) {
                                $('input').iCheck({
                                                checkboxClass: 'icheckbox_square-blue',
                                                radioClass: 'iradio_square-blue',
                                                increaseArea: '20%' // optional
                                });
                                
                                var i=0;
                                $('#theform .iradio_square-blue').each(function(){
                                                //alert($('#theform span:eq('+i+')').height());
                                                if ($('#theform span:eq('+i+')').height()>40)
                                                {
                                                                                                                $(this).css("top",$('#theform span:eq('+i+')').height()/4);
                                                }
                                                i++;
                                });
                }



	$("#submitButton").click(function(){
		var questiontype = {$questionType};
		var flag = false;

		if(questiontype==2){
			var maxanswers = {$maxanswers};
			var checkedanswers = 0;

			$(':checkbox').each(function() {
    				if(this.checked) checkedanswers += 1;
			});	
	
			if(checkedanswers == 0) flag = true;
			if(checkedanswers > maxanswers) flag = true;

			if(!flag){
				$("#theform").submit();	
			}else { 
				alert("You did not specify a valid number of responses!");
			}
		}

		if(!flag){
                	$("#theform").submit(); 
                }else { 
			alert("You did not specify a valid number of responses!");
                }

	});
});
</script>
</body>
</html>
