<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Best Paper Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>Welcome to the Best Paper Election Page</h1>
</div>
<div id = "maintext">
<p>Unfortunately the submission period has not yet begun for this election. Please check the e-mail you were sent by the election co-ordinator to see when the submission period will begin.</p>
</div>
</div>
</body>
</html>
