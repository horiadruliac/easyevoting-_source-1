<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Administration Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Submit Paper Page</h1>
</div>
<div id = "maintext">
<p> Papers Already Submitted:</p>
<table border='1'>
<tr>
<th> Paper ID </th> <th> Paper Title </th> <th> Change Details </th>
</tr>
{foreach from=$papers item=paper}
<tr>
<td>{$paper.id}</td>
<td>{$paper.title} </td> 
<td><a href = "submitpaper.php?id={$submissionset}&paperid={$paper.id}">Change Details</a></td>
</tr>
{/foreach}
</table>
<p> <a href = "submitpaper.php?id={$submissionset}&paperid=-1">Submit New Paper</a></p>
<p> <a href="viewsubmission.php?submissionset={$submissionset}">View existing submissions</a></p>
</div>
</div>
</body>
</html>
