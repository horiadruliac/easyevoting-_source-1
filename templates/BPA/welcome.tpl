<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id="page">
<div id="masthead">
<h1>Welcome to the Best Paper Award Election Page</h1>
</div>
<div id = "maintext">
<p>We host the following current and recent elections: </p>
<table border="1">
<tr>
<th>Election Title</th><th>Paper Submission</th><th>Election</th><th>View Election Questionaire Results</th>
</tr>
{foreach from = $submissionpages item = submission}
<tr>
{if ($submission.type == "opensubmission")}
<td> <a href = "viewsubmission.php?submissionset={$submission.id}">{$submission.title}</a>  </td>
<td> In progress - ends on {$submission.enddate} </td>
<td> </td> 
<td> Not yet available </td>
{elseif ($submission.type == "electiondue")}
<td> <a href="main.php?id={$submission.id}">{$submission.title}</a>  </td>
<td> Ended on {$submission.enddate} </td>
<td> Due to start on {$submission.startdate}</td> 
<td> Not yet available </td>
{elseif ($submission.type == "openelection")}
<td> <a href="main.php?id={$submission.id}">{$submission.title}</a>  </td>
<td> Ended </td>
<td> In progress and due to end on {$submission.enddate}</td> 
<td> <a href="bestpaperresults.php?election={$submission.id}">View Questionaire Results</a> </td>
{elseif ($submission.type == "electionended")}
<td> <a href="main.php?id={$submission.id}">{$submission.title}</a>  </td>
<td> Ended </td>
<td> Ended on {$submission.enddate}</td>
<td> <a href="bestpaperresults.php?election={$submission.id}">View Questionaire Results</a> </td>
{elseif ($submission.type == "noelectionyet")}
<td> <a href = "viewsubmission.php?submissionset={$submission.id}">{$submission.title}:</a>  </td>
<td> Ended on {$submission.enddate} </td>
<td> Will be announced soon</td> 
<td> Not yet available </td>
{/if}
</tr>
{/foreach}
</table>
</div>
</div>
</body>
</html>
