<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Delete Paper</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Delete Paper</h1>
</div>
<div id = "maintext">
<h2> Confirm Delete</h2>
<p><strong>Are you sure you wish to delete paper {$paperid}?<p>
<p><strong><a href="submitpaper.php?option=delete&id={$submissionset}&paperid={$paperid}&sure=yes">Yes</a><p>
<p><strong><a href="submitpaper.php?id={$submissionset}">No</a><p>
</div>
</div>
</body>
</html>
