<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Best Paper Election Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Submission Page</h1>
</div>
<div id = "maintext">
Your paper has been submitted with the following details.
<h2> {$title}</h2>
<p><strong>Bibliographic details:</strong> {$biblio}<p>
<p><strong>Impact Statement:</strong> {$testimony}<p>
<p><a href="viewpaper.php?submissionset={$submissionset}&id={$id}"> View Paper </a></p>
<p><a href="submitpaper.php?id={$submissionset}&paperid={$id}"> Change Details </a></p>
<p><a href="viewsubmission.php?submissionset={$submissionset}"> View All Submissions </a></p>
</div>
</div>
</body>
</html>
