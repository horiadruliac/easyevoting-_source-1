<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Co-ordinator Page</title>
<link href="coordinatorstyle.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div id="page">
<div id="masthead">
<h1>Co-ordinator Page</h1>
</div>
<div id="maintext">
<p>You can peform the following actions.</p>
<p><a href="createsubmissions.php">Create the submission page for a new election</a></p>
<p><a href="submissionadmin.php">Manage an existing submission page (including creating the election after submissions have ended)</a></p>
<p><a href="electionadmin.php">Manage an existing election</a></p>
</div>
</div>
</body>
</html>
