<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Election Administration Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
</head>
<body>
<div id = "page">
<div id = "masthead">
<h1>Papers Submitted for the Best Paper Award</h1>
</div>
<div id = "maintext">
<p> Papers Submitted:</p>
<table border='1' class="wrapped">
<tr>
<th style="width: 5%"> ID </th> <th> Title </th> <th> Bibliographic Details </th> <th> Impact Statement </th> <th style="width: 9%"> View Paper </th> <th style="width: 11%"> Change Paper Details </th>
</tr>
{foreach from=$submissions item=submission}
<tr>
<td> {$submission.id} </td> 
<td>{$submission.title} </td> 
<td>{$submission.biblio} </td>
<td>{$submission.testimony} </td>
<td><a href="viewpaper.php?submissionset={$submissionset}&id={$submission.id}"> View Paper </a> </td> 
<td> <a href="submitpaper.php?id={$submissionset}&paperid={$submission.id}">Change Paper Details</a></td>
</tr>
{/foreach}
</table>
<p> <a href="submitpaper.php?id={$submissionset}">Submit a paper</a></p>
</div>
</div>
</body>
</html>
