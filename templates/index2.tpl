<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Warwick E-voting System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="">

        <link href="./css/indexstyle.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="./js/html5shim.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="main" class="container-narrow">
            <!-- Main hero unit for a primary marketing message or call to action -->
            <h1 id="title">Warwick E-Voting</h1>
            <h2 id="sub">Enter an Election ID and passcode to start voting</h2>

            <div id="main-form-group">
                <!-- Start voting form -->
                <form id="main-form" name="myform" action="login.php" method="post" class="form-horizontal">
                    <fieldset>
                        <!-- Text input-->
                        <div class="control-group">
                            <label for="sessionID" class="control-label form-label">Election ID</label>
                            <div class="controls">
                                <input id="sessionID" name="sessionID" placeholder="Enter ID here..." required="" class="span3 text-input input-large" type="text" />
                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="control-group">
                            <label for="passcode" class="control-label form-label">Passcode</label>
                            <div class="controls">
                                <input id="passcode" name="passcode" placeholder="Enter passcode here..." class="span3 text-input input-large" type="password" />
                            </div>
                        </div>

                        <!-- Buttons -->
                        <div id="button-area">
                            <input id="big-button" type="submit" value="Start Voting" class="btn btn-success btn-large" />
                        </div>

                        <div id="sub-buttons">
                            <input id="view-results" type="button" value="View Results" class="btn" />
                            <input id="view-bulletin-board" type="button" value="Bulletin Board" class="btn" />
                        </div> 
                    </fieldset>
                </form>

            </div>




            <footer>
                <a class="main-button" href="help.php"><img style="width: 23%;max-width: 180px;" class="front" src="./img/help.png" alt="Help"></a>
                <a class="main-button" href="https://play.google.com/store/apps/details?id=uk.ac.ncl.evoting&amp;hl=en"><img class="front" style="width: 23%;max-width: 180px;" src="./img/google_play.png" alt="Google Play Link"></a> 
                <a class="main-button" href="https://itunes.apple.com/gb/app/id565080670?mt=8&amp;affId=1744357"><img class="front" style="width: 23%;max-width: 180px;" src="./img/app_store.png" alt="Apple App Store Link"></a>
                <a class="main-button"  href="manage.php"><img class="front" 	style="width: 23%;max-width: 180px;" src="./img/coord1.png" alt="Coordinator"></a>
            </footer>





        </div>
        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#view-results").click(function () {
                    var intRegex = /^\d+$/;
                    if ($("#sessionID").val() == "" || !intRegex.test($("#sessionID").val())) {
                        alert("Please enter a valid Election ID");
                    } else {
                        //Change form action and then submit
                        $("#sessionID").attr("name", "sessionid");
                        $("input#passcode").attr("disabled", "disabled");
                        $("form").attr("method", "get");
                        $("form").attr("action", "allresults.php");
                        $("form").submit();
                        //Reenable
                        resetfields();
                    }
                });

                $("#view-bulletin-board").click(function () {
                    var intRegex = /^\d+$/;
                    //Get question ID
                    if ($("#sessionID").val() == "" || !intRegex.test($("#sessionID").val())) {
                        alert("Please enter a valid Election ID");
                    } else {
                        var questionid = prompt("Please enter the question ID");

                        if (intRegex.test(questionid))
                        {
                            //Append new form element to form
                            $("<input>").attr({
                                type: "hidden",
                                name: "questionid",
                                id: "hidden-qid",
                                value: questionid
                            }).appendTo("form");
                            $("#sessionID").attr("name", "sessionid");
                            $("input#passcode").attr("disabled", "disabled");
                            $("form").attr("method", "get");
                            $("form").attr("action", "classroombulletinboard.php");
                            $("form").submit();
                            resetfields();
                            $("#hidden-qid").remove();
                        } else
                            alert("Please enter a valid question ID");
                    }
                });

                function resetfields()
                {
                    $("#sessionID").attr("name", "sessionID");
                    $("input#passcode").removeAttr("disabled");
                    $("form").attr("action", "./login.php");
                    $("form").attr("method", "post");
                }
            });
        </script>
    </body>
</html>
