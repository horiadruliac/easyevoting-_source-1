<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle University E-voting: Results Page</title>
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<style>
.results-btn { width: 140px; }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="../navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Election Results</h1>
<p class="lead">Title: {$sessionTitle}</p>
<p class="lead">Session ID: {$sessionID}</p>
</div>
<div id = "maintext">
{$questioncounter=1}
{foreach from=$results item = result}
<p> <strong>Question {$questioncounter++}:</strong> {$result.questiontext} </p>
{if $result.finished == false && ($result.type == "12")}
<p>Voting has not yet finished for this question</p>
<p><a class="btn" href="classroombulletinboard.php?sessionid={$sessionID}&questionid={$result.questionnumber}">View Bulletin Board</a></p>
{else}
{if $result.finished == false}
<p>Voting has not yet finished so more results may be added</p>
{/if}
<div>

{if $result.type == "4"}

{if isset($result.results)}
{$counter=1}
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Answer Number</th><th>Text</th>
</tr>
</thead>

{foreach from=$result.results item = subresult}
<tr>
<td>{$counter++}</td> <td> {$subresult.answer}</td>
</tr>
{/foreach}
</table>
{/if}
{else}
{if isset($result.results)}
{$counter=1}
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>Answer Number</th><th>Text</th><th>Votes</th>
</tr>
</thead>
{foreach from=$result.results item = subresult}
<tr>
<td>{$counter++}</td> <td>{$subresult.answer}</td><td>{$subresult.votes}</td>
</tr>
{/foreach}
</table>
{/if}
{/if}

{if $result.type == "12" || $result.type == "3" || $result.type == "5"}
<div>
<img src="generategraph.php?sessionid={$sessionID}&questionid={$result.questionnumber}" alt="Graph of results"/>
</div>

{/if}
{if $result.type == "12" || $result.type == "5"}
<p><a class="results-btn btn" href="classroombulletinboard.php?sessionid={$sessionID}&questionid={$result.questionnumber}">View Bulletin Board</a></p>
{else}
<p><em>Note: Type 3 and 4 questions do not have bulletin board pages.</em></p>
{/if}
</div>
{/if}


<br/>
{/foreach} 

</div>
</div>

<script src="./js/jquery-1.9.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>

</body>
</html>
