 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Newcastle University E-Voting</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/mainstyle.css"/>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->
</head>
<body>
{include file="./navbar.tpl"}
<div class="container-narrow">
<div class="jumbotron">
<h1>Vote Status</h1>
</div>
<div class="pagination-centered">
<div id="receiptholder">
<h2>Confirmed Ballot</h2>
<h4>Session ID: {$sessionID}</h4>
{foreach from=$receipts item=receipt}
<h4>Question ID: {$receipt.questionID}</h4>
<h4>Ballot ID: {$receipt.ballotnumber}</h4>
<h3>Receipt: <strong id='receipt'>{$receipt.value}</strong></h3>
<p><a class="btn btn-large" href="classroombulletinboard.php?sessionid={$sessionID}&questionid={$receipt.questionID}&ballot={$receipt.ballotnumber}" target="_blank">Verify Your Receipt</a></p></p>
{/foreach}
<hr/>
<p><a class="btn btn-large btn-primary" href="index.php">Return Home</a></p>
</div>
</div>
</div>
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/splitter.js"></script>
</body>
</html>
