<!DOCTYPE html> 
<html lang="en">
<head>
<title>Newcastle University E-Voting: View All Elections</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./css/bootstrap-responsive.php" />
<link rel="stylesheet" type="text/css" href="./css/extra-css.css" />
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./js/html5shim.js"></script>
<link rel="stylesheet" type="text/css" href="./css/ie8.css" />	  
    <![endif]-->

</style>
</head>
<body>

{include file="../navbar.tpl"}



<div class="container-narrow">
<div class="jumbotron">
<h1>Public Elections</h1>
<p class="lead">View the status, creator and election ID of every election</p>
</div>
<div id = "maintext">
{if isset($sessions)}
<div>
<table class="table table-condensed table-striped">
<thead>
<tr>
<th>ID</th><th>Status</th><!--<th>Passcode Type</th><th>Passcode</th>--><th>Creator</th><th>Title</th>
{if isset($admin)}
<th>Action</th>
{/if}
</tr>
</thead>
<tbody>
{foreach from=$sessions item = session}
<tr> <td> <a class="btn btn-primary" href="sessionlist.php?sessionid={$session.id}">{$session.id}</a> </td> <td> 
{if $session.generated}
{if $session.locked}
Locked
{elseif $session.finished}
Finished
{else}
Ready
{/if}
{else}
Generating
{/if}

</td>
<td>
{$session.username}
</td>
<td>
{$session.title}
</td>
{if isset($admin)}
<td>
<select onchange="coordaction(this)">
<option value="select">Select Action</option>
{if $session.hidden}
<option value="unhidesession({$session.id})" >Unhide Session</option>
{else}
<option value="hidesession({$session.id})" >Hide Session</option>
{/if}
</select>
</td>
{/if}
</tr>


{/foreach} 
</tbody>
</table>
</div>
<a id="electionend"> </a>
{if $moreelections}
<div><a href="sessionlist.php?showelections={($showelections+20)}#electionend">Show More Elections</a></div>
{/if}
{else}
<div class="jumbotron">
<h2> There are currently no sessions available.</h2>
</div>
{/if}
<br/>
</div>
</div>

<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/sessionlist.js"></script>
</body>
</html>
